const http = require("http");

const port = 5000;

//mock database
let directory = [
	 {
                "firstName": "Mary Jane",
                "lastName": "Dela Cruz",
                "mobileNo": "09123456789",
                "email": "mjdelacruz@mail.com",
                "password": 123
        },
        {
                "firstName": "John",
                "lastName": "Doe",
                "mobileNo": "09123456789",
                "email": "jdoe@mail.com",
                "password": 123
        }
]

console.log(typeof directory)


const server = http.createServer((request, response) => {

	if(request.url == "/profile" && request.method == "GET") {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.parse(directory));
		res.end("Data retrieved from the database")
	}

        if(request.url == "/post" && request.method == "POST") {

                let requestBody = '';

                request.on('data', function(data){
                        requestBody += data;

                });

                request.on('end', function() {
                        console.log(typeof requestBody);

                        requestBody = JSON.parse(requestBody)

                        let user = {
                                "firstName": requestBody.firstName,
                                "lastName": requestBody.lastName,
                                "mobileNo": requestBody.mobileNo,
                                "email": requestBody.email,
                                "password": requestBody.password
                        }

                        directory.push(user);
                        console.log(directory)

                        response.writeHead(200, {'Content-Type': 'application/json'});
                        response.write(JSON.stringify(user));
                        response.end()
                })
        }



});

server.listen(port);

console.log(`Server running at localhost:${port}`);